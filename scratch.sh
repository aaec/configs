#! /bin/bash

#example command
#bar_status_command='while echo "BAT1: $bat1_info"; do sleep 1; done'
#function get_bat_stats {
	#bat_status=$(upower -i /org/freedesktop/UPower/devices/battery_BAT$1 | grep state | cut -d ":" -f 2 | tr -d [:space:])
	#bat_status=$(upower -i /org/freedesktop/UPower/devices/battery_BAT$1)
	#echo "$bar_status"
	#bat_status=$(test $bat1_status "discharging" && echo $bat1_status)

	#bat_percentage=$(upower -i /org/freedesktop/UPower/devices/battery_BAT$1 | grep percentage | cut -d ":" -f 2 | tr -d [:space:])
	#echo "BAT$1: $bat_percentage $bat_status"
#}

while (
	bat1_status="$(upower -i /org/freedesktop/UPower/devices/battery_BAT1 | grep state | cut -d ":" -f 2 | tr -d [:space:])"
	bat1_status=$(test $bat1_status != 'discharging' && echo $bat1_status)

	bat1_percentage=$(upower -i /org/freedesktop/UPower/devices/battery_BAT1 | grep percentage | cut -d ":" -f 2 | tr -d [:space:])

	volume=$(amixer get Master | grep -o -e "..%" -m 1)

	echo "VOL: $bat1_status $(date +'%a %d %b %y %R:%S %Z') $(hostname)")
do sleep 1; done
