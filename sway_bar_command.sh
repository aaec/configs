#! /bin/bash
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#Author: Ava Cole <aaec@uw.edu> 
#Source: gitlab.com/aaec
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

# ----------- FUNCTIONS ---------
# get information from upower on a battery device.
function bat_stats {
	bat_status=$(upower -i /org/freedesktop/UPower/devices/battery_BAT$1 | grep state | cut -d ":" -f 2 | tr -d [:space:])
	# we include the status only if it's not discharging
	bat_status=$(test "$bat_status" != 'discharging' && echo $bat_status)

	bat_percentage=$(upower -i /org/freedesktop/UPower/devices/battery_BAT$1 | grep percentage | cut -d ":" -f 2 | tr -d [:space:])
	# echo "BAT$1: $bat_percentage $bat_status"
	echo "BAT$1: $bat_percentage"
}

# get the current access point associated with wireless interface and show its
# stats
function wifi_stats {
	# helper function to extract data from nmcli output and format
	function extract {
		echo $(echo "$1" | grep $2 | cut -d ":" -f 2 | awk '{$1=$1};1')
	}

	# get the section of the output related to the AP currently in use and
	# pull out the different bits of info we want

	# Another way to get ap name:
	#ap_name=$(nmcli connection show --active | awk ' BEGIN { FS="  " } NR == 2 { print $1 }')

	nmcli_output=$(nmcli -f AP device show | grep --after-context=7 "IN-USE.*\*")
	# if the output of that command wasn't zero
	if [ -n "$nmcli_output" ]; then
		ap_name=$(extract "$nmcli_output" 'SSID')
		ap_rate=$(extract "$nmcli_output" "RATE")
		ap_bars=$(extract "$nmcli_output" 'BARS')
		ap_signal=$(extract "$nmcli_output" 'SIGNAL')
		# echo "$1 -> $(echo $ap_name | cut -c 1-12) $ap_rate $ap_signal"
		echo "$1: $ap_signal"
	else
		echo "$1: null"
	fi
}

# get the current volume percentage
function volume {
	echo $(amixer get Master | grep -o -e "..%" -m 1)
}

# ----------- MAIN -------------

#example command
#bar_status_command='while echo "BAT1: $bat1_info"; do sleep 1; done'

while (
	echo -n -e "$(wifi_stats 'wlp3s0')\tVOL: $(volume)\t$(bat_stats '1') $(bat_stats '0')\t$(date +'%a %d %b %y %R:%S %Z') $(hostname)")
do sleep 1; done
