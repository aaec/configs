set nocompatible
" behavior: {{{
" Backups: {{{

"enable backups and not swap files cuase swap files are dumb
set backup
set noswapfile
" set location for backup, undo, and swapfiles
set backupdir=~/.vim/tmp/backup//
set undodir=~/.vim/tmp/undo//
set directory=~/.vim/tmp/swap//
" make those folders if they dont already exist
if !isdirectory(expand(&backupdir))
    call mkdir(expand(&backupdir), "p")
endif
if !isdirectory(expand(&directory))
    call mkdir(expand(&directory), "p")
endif

"}}}
" Tabs: {{{

set tabstop=4
set shiftwidth=4
set softtabstop=4
" allow backspacing over autoindent, line breaks and start of insert action
set backspace=indent,eol,start
"intelligently determines indent on new lines
set autoindent smartindent copyindent
" draw lines after tabs
set listchars=tab:\|\ "dont touch this white space :>
"set listchars=tab:»-,nbsp:·,eol:¶,trail:§

"}}}
" Remaps: {{{

let mapleader = "="
"clean file of trailing whitespace :>
nnoremap <leader>c :execute "%s/".'\v('."<space><bar><tab>".')+$'."//gc"<cr>
" edit and source vimrc fast draw
nnoremap <leader>ev :edit $MYVIMRC<cr>
nnoremap <leader>sv :source $MYVIMRC<cr>
" go to previous and next location in history with f and b
nnoremap <leader>f <C-i>
nnoremap <leader>b <C-o>
" GOTTA GO FAST NO ESCAPE LIFE IS PAIN GO GO GOJJKJKJK
inoremap jk <esc>
" list gnarly CHARlies with <leader>l
map <leader>l :set list!<cr>
" SPelling errors
map <leader>sp :setlocal spell! spelllang=en_us<cr>
" insert a blank line below or above current line
nnoremap ' o<esc>k
nnoremap " O<esc>j
" stupid fucking latex garbage I want to drop out and or die
nnoremap <leader>mod :%s/\\mod/\\text{ mod }/g<cr>
" write and close a buffer (vimrc) fast
nnoremap <leader>wq :<c-u>execute 'write<bar>bd'<cr>
" auto closes brackets when you type { ENTER
inoremap {<cr>    {<cr>}<Esc>O<backspace>
" force clear highlights on pressing esc in normal mode, v sloppy
nnoremap <esc> :let @/ = ""<bar>match none<cr>
" open folds with space!! yeahh babyeee
nnoremap <space> za
" Write a readonly file using sudo
cmap w!! %!sudo tee > /dev/null %

"}}}
" Abbreviations: {{{

" make headers ez
iabbrev @@ ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~<cr>Author: Ava Cole <aaec@uw.edu> <cr>Source: gitlab.com/aaec<cr>~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

" }}}
" Folding: {{{

set foldenable
set foldlevelstart=20
set foldnestmax=12
set foldmethod=indent

"}}}
" File Type Specific: {{{

" HTML
autocmd FileType html set tabstop=2 | set shiftwidth=2 | set softtabstop=2
"CONF
augroup filetype_conf
	autocmd!
	autocmd FileType vim,conf setlocal foldmethod=marker foldlevel=-1
augroup END
" TEXT
augroup filetype_text
	autocmd!
	autocmd FileType text,markdown setlocal nonumber norelativenumber linespace=10
	autocmd FileType text,markdown setlocal noautoindent nosmartindent textwidth=0
	autocmd FileType text,markdown setlocal linebreak
	autocmd FileType text,markdown nnoremap <buffer> j gj
	autocmd FileType text,markdown nnoremap <buffer> k gk
augroup END

"}}}
" Tags and Search: {{{

" Highlight all search results
set hlsearch incsearch showmatch
" case insensitive except when smartcase
set ignorecase
" Enable smart-case search
set smartcase infercase

"}}}
" Mouse is witch craft I know it's tempting but don't touch
"set mouse=a
"don't blink or beep at me
set belloff="all"
set showmatch
" determines where new windows appear when :split and :vsplit are used
set splitbelow splitright
" makes the title reflect the file being modified
set title
" resize splits when window is resized needs work
autocmd VimResized * :wincmd =
set confirm
" dont show trailing spaces in shit
set nolist
" Keep 5 lines above/below and left/right when scrolling
set scrolloff=5
set sidescrolloff=5
" sets wrap width
set textwidth=0
set wrap
set paste
" }}}
" Appearance: {{{
" Theme: {{{

syntax on

"}}}
" Status Bar: {{{

set laststatus=2
set statusline=
set statusline+=%3*
set statusline+=\ 
set statusline+=%{StatuslineMode()}
set statusline+=\ 
set statusline+=%2*
set statusline+=\ 
set statusline+=%f
set statusline+=\ 
set statusline+=%9*
set statusline+=%=
set statusline+=%y
set statusline+=\ 
set statusline+=%2*
set statusline+=\ 
set statusline+=%l
set statusline+=/
set statusline+=%L
set statusline+=\ 
hi User3 ctermbg=white ctermfg=black guibg=white guifg=black
hi User2 ctermbg=lightblue ctermfg=black guibg=lightyellow guifg=black
hi User9 ctermbg=black ctermfg=white guibg=black guifg=white

function! StatuslineMode()
  let l:mode=mode()
  if l:mode==#"n"
    return "NORMAL"
  elseif l:mode==?"v"
    return "VISUAL"
  elseif l:mode==#"i"
    return "INSERT"
  elseif l:mode==#"R"
    return "REPLACE"
  elseif l:mode==?"s"
    return "SELECT"
  elseif l:mode==#"t"
    return "TERMINAL"
  elseif l:mode==#"c"
    return "COMMAND"
  elseif l:mode==#"!"
    return "SHELL"
  endif
endfunction




"}}}
set nonumber norelativenumber
"makes line n relative when ur in normal mode in active buffer and not otherwise
"augroup numbertoggle
"	autocmd!
"	autocmd BufEnter,FocusGained,InsertLeave * set norelativenumber
"	autocmd BufLeave,FocusLost,InsertEnter * set relativenumber
"augroup END
" highlights 101st char of each line to let u know ur a messy no good coder
"highlight ColorColumn ctermbg=magenta
call matchadd('ColorColumn', '\%101v', 100)
" }}}
